BASIC TYPES

For programs to be useful, we need to be able to work with some of the simplest units of data: numbers, strings, structures, boolean values, and the like. In TypeScript, we support much the same types as you would expect in JavaScript, with a convenient enumeration type thrown in to help things along.

The most basic datatype is the simple true/false value, which JavaScript call a `boolean` value.

```javascript
let easy = true;
```

# EXERCISE

Create a boolean variable `javascriptIsEasy` and set its value to `true`.

Create a person object and set it with name jhon , age 35.

--

## HINTS

When you are done, you must run:

```sh
$ javascriptworkshop verify script.js
```

to proceed. Your script will be tested, a report will be generated, and the les
