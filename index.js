const workshopper = require("workshopper-adventure");
const path = require("path");

var javascriptworkshop = workshopper({
  title: "JavascriptWorkshop",
  appDir: __dirname,
  exerciseDir: path.join(__dirname, "exercises"),
  languages: ["en"]
});
javascriptworkshop.addAll(["variable"]);
module.exports = javascriptworkshop;
//© 2019 GitHub, adnen rebai
